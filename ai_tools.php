<?php
/**
 * @copyright  Copyright (c) 2021 François Elie & Marin Elie
 * @license    http://opensource.org/licenses/AGPL-3.0 AGPL-3.0
 * @link       https://gitlab.adullact.org/felie/ai
 */

/* ============================== gestion des zip pour peupler des tables avec des liens vers des fichiers ================= */
 
function get_zip(){ // populate a table with the file in a zip
    // complex: table1=authors,table2=opus with an id_auteur in it
    //         "auteur - titre.typ" 
    //    =>   add auteur in auteurs
    //       + add titre with id_auteur and a link "url"
    // simple: "name - titre.typ
    //    => in a table with his url
    return "<h3>Dépliage d'un zip en base de données</h3>
    
    Indiquez les noms de table au singulier<br/>
    Le mode simple ajoute à une table (ne pas renseigner le nom de la deuxième table)<br/>
    Le mode complexe traite des fichiers dont le nom est de la forme 'A - B.ttt' 
    Il ajoute A aux T1s, et ajoute B aux T2s avec un lien (id_T1) qui va bien.<br/>
    
    Dans les deux modes, un champ url indique l'adresse relative du fichier physique à partir du répertoire upload d'ai
    
    <form method='post' action='' enctype='multipart/form-data'>
    <input type='hidden' name='ai_zip'/>
    <table>
        <tr><td align=right>table n°1:</td><td><input type='text' name='table1' value=''/></td></tr>
        <tr><td align=right>table n°2:</td><td><input type='text' name='table2' value=''/></td></tr>
        <tr><td colspan=2 align=right><input type='file' name='zip'/></td></tr>
    <tr class=noborder><td class=noborder colspan=2 align=right><input type='submit' value='ok'/></td></tr>
    </form>";
}

function ai_unzip(){
    global $ai_upload,$ai_uploaddir;
    //show($_POST);
    extract($_POST);
    $mode = ($table2 == '') ? 'simple' : 'complex';
    $file=$_FILES['zip']['tmp_name'];
    //echo "file=".$file.'<br/>';
    $name=basename($file);
    $dir=dirname($file);
    $workdir="$ai_uploaddir/$name".'ZIP';
    mkdir($workdir);
    if (move_uploaded_file($file,"$workdir/$name")){
        //echo 'déplacement du zip réussi<br/>';
        exec("cd $workdir;unzip $name"); // dezippage des fichiers
        unlink("$workdir/$name"); // effacement du ai_zip
        $liste = glob("$workdir/*");
        //show($liste,'liste');
        switch($mode){
            case 'simple':
                foreach($liste as $path){
                    $fichier=basename($path);
                    $dir = "$ai_upload/$table1";
                    $info = pathinfo($fichier);
                    $titre =  basename($fichier,'.'.$info['extension']);
                    $q="INSERT IGNORE INTO $table1"."s ($table1,url) VALUES('$titre','$dir/$fichier');";
                    if (!file_exists($dir)){
                        mkdir($dir);
                    }
                    rename($path,"$ai_uploaddir/$table1/$fichier");
                    echo "q=$q<br/>";
                    ai_query($q);
                    }
                break;
            case 'complex':
                echo "table2=$table2<br>";
                foreach($liste as $fichier){
                    $dir = "$ai_upload/$table2";
                     if (!file_exists($dir)){
                        mkdir($dir);
                    }
                    $name=explode(' - ',basename($fichier));
                    rename($fichier,"$ai_uploaddir/$table2/".basename($fichier));
                    $q="INSERT IGNORE INTO $table1"."s ($table1) VALUES ('$name[0]');"; // ajoute s'il n'est pas présent
                    echo "q = $q<br/>";
                    ai_query($q);
                    $id1=ai_simple_query("SELECT id FROM $table1"."s WHERE $table1='$name[0]';")[0];
                    $q="INSERT IGNORE INTO $table2"."s ($table2,id_$table1) VALUES ('$name[1]',$id1);"; // ajoute s'il n'est pas présent
                    echo "q = $q<br/>";
                    ai_query($q);
                }
                    
            }
        }else{
    error("Échec du déplacement du zip");
    }
}

/* ================================ action pour zip ===================================== */

if (isset($_POST['ai_zip'])){
    ai_unzip();
}

/* ================== aide à la construction de base de données (MPD automatique) ======================================== */

function split_clever($s,$sep){
    $result=[];
    $temp='';
    $prof=0;
    for ($i=0;$i<strlen($s);$i++){
        $c=$s[$i];
        if ($c=='(' or $c=='{' or $c=='['){
            $prof++;
        }
        if ($c==')' or $c=='}' or $c==']'){
            $prof--;
        }
        if ($prof==0 and $c==$sep and $i>0){
            $temp=trim($temp);
            if ($temp!='')
                $result[]=$temp;
            $temp='';
        }else{
            $temp.=$c;
        }
    }
    $temp=trim($temp);
    if ($temp!='')
        $result[]=$temp;
    return $result;
}

// la fonction produit les requêtes sql de création et le schéma graphviz :)

function ai_get_mpd(){
    return "## Modélisation de données rapide  

Décrivez vos tables de la manière suivante: truc,id_machin,id_bidule,

La table trucs va être crée, avec les champs id,truc,id\_machin et id\_bidule, truc est unique et id\_machin et id\_bidule sont des clés étrangères avec restriction sur les tables machins et bidules.<br/>

À part le premier champ qui est un varchar(100) est qui est unique, les autres variables (hors id_) sont par défaut des tinytext,ou alors il faut spécifier le type après un espace (inutile de mettre entre guillements pour les enum et les set

Le système produira en retour
- Les requêtes pour la création des tables en mysql
- Le graphique graphviz avec les liens entre les tables

Vous pouvez essayer avec l'exemple proposé.
<form method='post' action='' enctype='multipart/form-data'>
  <input type='hidden' name='ai_mpd'/>
    <table border=1 class='ai_table'>
        <tr class=noborder><td class=noborder><textarea rows=7 cols=100 name='texte' value='votre description ici'>
classe
eleve,id_eleve_ami,id_classe,age shortint,taille shortint
demo,choix enum(fromage,dessert),habits set(cravate,short,charentaise)
prof

        </textarea></td></tr>
        <tr class=noborder><td class=noborder align=right>Générer des données bidon pour les tests ? <input type=checkbox name='dummy'/></td></tr>
        <tr class=noborder><td class=noborder align=right>Créer les tables et les peupler avec ces données ? <input type=checkbox name='tests'/></td></tr>
       <tr ><td class=noborder align=right><input type='submit' value='ok'/></td></tr>
    </table>
    </form>";
}

function ai_produce_generation_query($text,$dummy=false){
    $t=explode("\n",$text);
    $query_creation="
CREATE TABLE %s (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
%s
)
ENGINE=InnoDB;\n\n";
    $graph="digraph G
{
    rankdir = LR;
    %s
    %s
    }";
    $node='%s
    [
        shape = none
        label = <<table border="0" cellspacing="0">
        %s
        </table>>
        ]';
    $r='';
    $g='';
    $links=[];
    $bidon=[];
    $unknown=[];
    foreach ($t as $l){
        $l=trim($l);
        if ($l!=''){
            $fk=[];
            $fields=[];
            $lines=[];
            //$c = explode(',',$l);
            $c=split_clever($l,',');
            //show($c);
            $name = trim($c[0]).'s';
            //echo "name=$name";
            $n=0;
            $lines[]="<tr><td port=\"$name\" border=\"1\" bgcolor=\"gray\">$name</td></tr>";
            $lines[]="<tr><td port=\"id\" border=\"1\">id</td></tr>";
            $dummyfield=[];
            foreach ($c as $f){
                $n++;
                if (substr($f,0,3) == 'id_'){
                    $fields[]="  $f INT UNSIGNED DEFAULT 1";
                    $net=str_replace('_','',$f);
                    $lines[]="<tr><td port=\"$net\" border=\"1\">$f</td></tr>";
                    $x=explode('_',$f);
                    $dummyfield[]=$f;
                    $fields[]="  CONSTRAINT $name"."_fk_$f FOREIGN KEY ($f) REFERENCES $x[1]"."s(id)";
                    $links[]="$name:$net -> ".$x[1].'s:id';
                }else{
                    if (preg_match('/^(.*) (.*)$/U',$f,$rp)){
                        //show($rp,'rp');
                        $rp[2]=str_replace(array('(',',',')'),array('("','","','")'),$rp[2]);
                        $fields[]="  $rp[1] $rp[2]";
                        $lines[]="<tr><td port=\"$rp[1]\" border=\"1\">$rp[1]</td></tr>";
                    }else{
                        if ($n==1)
                            $fields[]="  $f VARCHAR(100) NOT NULL UNIQUE";
                        else
                            $fields[]="  $f TINYTEXT";
                        $dummyfield[]=$f;
                        $champ=$f;
                        $lines[]="<tr><td port=\"$f\" border=\"1\">$f</td></tr>";
                    }
                }
            }
        $unknown[].="INSERT into $name ($c[0]) VALUES ('');";
        }
        if ($dummy){
            for($i=0;$i<100;$i++){
                $value=[];
                foreach($dummyfield as $d){
                    $value[]=random_int(1,100);
                }
                $value[0]="'$champ".' n° '.$i."'";
                $bidon[]="INSERT IGNORE INTO $name (".join(',',$dummyfield).") VALUES (".join(',',$value).");";
            }
        }
        $r.=sprintf($query_creation,$name,implode(",\n",$fields));
        $g.=sprintf($node,$name,implode("\n",$lines))."\n";
    }
    $g=sprintf($graph,$g,implode("\n",$links));
    file_put_contents('tmp/graph.txt',$g);
    unlink('/web/ai/tmp/graph.png');
    exec('cd /web/ai/tmp/;dot -Tpng graph.txt > graph.png');
    
    return $r.join("\n\n",$unknown).join("\n\n",$bidon);
}

function ai_array2html($a){
    if ($a!=[]){
        $r='<table class="ai_table">';
        foreach ($a as $line){
            $r.='<tr>';
            if (is_array($line)){
                foreach ($line as $c){
                    $r.="<td>$c</td>";
                }
            }else{
                $r.="<td>$line</td>";
            }
        }
        return $r.'</table>';
    }
}

/* ================================== action (MPD) =============================================== */

if (isset($_POST['ai_mpd'])){
    //show($_POST);
    $queries=ai_produce_generation_query($_POST['texte'],isset($_POST['dummy']));
    if (isset($_POST['tests'])){
        foreach(explode("\n\n",$queries) as $q){
            //echo "q=$q<br/>";
            ai_query($q);
        }
        $content.="Voici la liste des tables dans votre base de données<br/>";
        $content.=ai_array2html(ai_simple_query('SHOW TABLES;'));
    }
    //$content.="<div style='margin-left:20%;width:60%;background-color:lightblue'><xmp>$queries</xmp></div>    ";
    $content.="Voilà ce que le générateur a produit à votre place:
    ```
    $queries
    ```
";
    $content.="<img src='/ai/tmp/graph.png'/>
";
}

?>
