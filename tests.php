<?php

require_once('ai_config.php');
echo 'coucou1';
require_once('ai.php');
echo 'coucou2';

$q='SELECT duck,id_ville,ducks.id_ville>villes.id_fondateur>fondateurs.billy>architectes.architecte,ducks.naissance>villes.ville FROM ducks order by ducks.id LIMIT 10';
$q='SELECT name,age,id_ville,persons.id_ville>villes.fondateur>fondateurs.name,persons.id_ville_naissance>villes.ville FROM persons order by persons.name';
$q='select name,persons.id_ville_naissance>villes.fondateur>persons.name from persons order by persons.name';

//$q=sql2html('select nom,personnes.id_ville>villes.fondateur>fondateurs.name from personnes','select');

echo '<h1>Démo/tests de Ai</h1>';

echo '<h2>Avec wildcard * (avec jointure multiple automatique)</h2>';
$q='select * from personnes order by personnes.nom';
echo "<b>$q</b><br/><br/>";
echo "Select<br/>";
echo ai_sql2html($q,'select','v');
echo "Update<br/>";
echo ai_sql2html($q,'update','v');
echo "Create<br/>";
echo ai_sql2html("$q limit 1",'create','v');
 
echo '<h2>Requête ciblée sur des champs</h2>';
echo "Select<br/>";
$q='select nom,age from personnes';
echo "Select<br/>";
echo ai_sql2html($q,'select','v');
echo "Update<br/>";
echo ai_sql2html($q,'update','v');
echo "Create<br/>";
echo ai_sql2html("$q limit 1",'create','v');

echo '<h2>Requête avec jointures en cascade multiple</h2>';
echo "Select<br/>";
$q='select nom,personnes.id_ville>villes.fondateur>fondateurs.name from personnes limit 4';
echo "Select<br/>";
echo ai_sql2html($q,'select','v');
echo "Update<br/>";
echo ai_sql2html($q,'update','v');
echo "Create<br/>";
echo ai_sql2html($q,'create','v');
?>
