<?php
/**
 * @copyright  Copyright (c) 2021 François Elie & Marin Elie
 * @license    http://opensource.org/licenses/AGPL-3.0 AGPL-3.0
 * @link       https://gitlab.adullact.org/felie/ai
 */

/*TODO
  mettre le début de la page web optionnel - rejeté dans l'index ?
/*

/*============================================== indication des erreurs php ====================*/
 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*==========================xmp==================== utilities ====================*/

function error($s){
    echo "<div style='color:red;'>ERROR $s</div><br/>";
}

function dump($var):void{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
}

function show($var,$nom=''):void{
    echo "<br/><b>$nom</b><pre><xmp>";
    print_r($var);
    echo "</xmp></pre>";
}

function stripquote($s){
    if ($s[0]=="'")
        $s=substr($s,1,strlen($s)-2);
    return $s;
}   

function str2array($s){ // from return array (recursive)
    $r=[];
    $temp='';
    $prof=0;
    if ($s[0] =='[' and $s[strlen($s)-1]==']'){
        if (strlen($s)>2){
            foreach (str_split(substr($s,1,-1)) as $c){
                if (in_array($c,['[','(','{']))
                    $prof++;
                else 
                    if (in_array($c,[')','}',']']))
                        $prof--;
                if ($prof != 0){
                    $temp.=$c;
                }else{
                    if ($c == ','){
                        $r[]=str2array(stripquote($temp));
                        $temp='';
                    }else
                        $temp.=$c;
                }

            }
            $r[]=str2array(stripquote($temp));
            return $r;
        }else
            return([]);}
    else
        return $s; 
}

//show(simple_array('[a,(b,d),[c,[g,k]]]'));
//exit;

/*============================================== début de la page web minimale et autonome ====================*/

$ai_header='<script src="ai.js"></script>
    <link rel="stylesheet" href="ai.css" />
    <!-- sorttable -->
    <script src="goodies/sorttable/sorttable.js"></script>
    <link rel="stylesheet" href="goodies/sorttable/sorttable.css" />
    <!-- sliders -->
    <link rel="stylesheet" href="goodies/sliders/sliders.css" />
    <!-- stars-input -->
    <link rel="stylesheet" href="goodies/stars-input/stars-input.css" />';

$ai_begin="
<!DOCTYPE html>
<html lang='fr'>
  <head>
    <meta charset='utf-8'>
    <title>ai, le retour</title>
    $ai_header
  </head>
  <body>";  
  
$ai_footer='<script src="goodies/sliders/sliders.js"></script>';

$ai_end="
$ai_footer
</body>
</html>";

//*============================================== constantes, variables et connexion base de données ====================*/

$ai_dsn = "mysql:host=$ai_host;dbname=$ai_db;charset=$ai_charset";

$ai_options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
try {
    $ai_pdo = new PDO($ai_dsn, $ai_user, $ai_pass, $ai_options);
} 
catch (\PDOException $ai_e) {
    throw new \PDOException($ai_e->getMessage(), (int)$ai_e->getCode());
}

$ai_envelop=[
'default_envelop' => [
"<table class='ai_table sortable'>\n%s\n</table>\n",
"  <tr>\n%s  </tr>\n",
"   <td>
       %s
    </td>\n",
"    <th>
       %s
    </th>\n"
],
'form_envelop' => [ 
    '<form method="post" enctype="multipart/form-data">
<AI_HIDDEN_DATA>
<input type="hidden" name="<AI_MOD>" value="whatever"/>
 <table class="ai_table">%s
  <tr class="noborder rightalign">
   <td class="noborder" style="background-color:inherit;" colspan=100%%>
    <input style="font-weight:bold;" type="submit" size=1 value="<AI_READABLE_MOD>"/>
   </td>
  </tr>
 </table>
 </form>',
    '<tr>%s
  </tr>',
    '<td>%s
</td>',
    '<th>%s
  </th>'
]
];

$ai_readable_mod=[
'insert' => 'ajouter',
'update' => 'mettre à jour',
'select' => ''
];

$ai_predefmask = [
    '-' => '%s',
    'h' => '<!--%s-->',
    'c' => '<center>%s</center>',
    'lien' => '<a href="%s">%s</a>',
    'b' => '<b>%s</b>',
    'comment' => '<!-- %s -->',
    'option' => '<option value="%s">%s</option>',
    'u' => '<a href="?id=%s&ai_action=update&table=<AI_TABLE>"><img src="/ai/images/edit.png"></a>',
    'd' => '<a href="?id=%s&ai_action=delete&table=<AI_TABLE>" onclick="return(confirm(\'Confirmation de l\'effacement de l\'enregistrement n°<AI_ID> ?\'))"><img src="/ai/images/del.png"/></a>',
    'ud' => '<a href="?id=%1$s&ai_action=update&table=<AI_TABLE>"><img src="/ai/images/edit.png"></a><a href="?id=%1$s&ai_action=delete&table=<AI_TABLE>" onclick="return(confirm(\'Confirmation de l\\\'effacement de l\\\'enregistrement n°%1$s ?\'))"><img src="/ai/images/del.png"/></a>',
    'stars' => '%s<div class="star-rating">
          <input type="radio" name="<AI_FIELD>[<AI_ID>][]" value="5" id="id<AI_TOKEN>-5">
          <label for="id<AI_TOKEN>-5">★</label>
          <input type="radio" name="<AI_FIELD>[<AI_ID>][]" value="4" id="id<AI_TOKEN>-4">
          <label for="id<AI_TOKEN>-4">★</label>
          <input type="radio" name="<AI_FIELD>[<AI_ID>][]" value="3" id="id<AI_TOKEN>-3">
          <label for="id<AI_TOKEN>-3">★</label>
          <input type="radio" name="<AI_FIELD>[<AI_ID>][]" value="2" id="id<AI_TOKEN>-2">
          <label for="id<AI_TOKEN>-2">★</label>
          <input type="radio" name="<AI_FIELD>[<AI_ID>][]" value="1" id="id<AI_TOKEN>-1">
          <label for="id<AI_TOKEN>-1">★</label>
        </div>',
    'range' => '<div class="range-wrap"><input type="range" name="<AI_FIELD>[<AI_ID>][]" value="%1$s" class="range"><span class="bubble">%1$s</span></div>',
    'f' => '<input type="file" name="<AI_FIELD>[<AI_ID>][]" value="%1$s"> %1$s',
    'file' => '<input type="file" name="<AI_FIELD>[<AI_ID>][]" value="%1$s"> %1$s',
    'files' => '<input type="file" name="<AI_FIELD>[<AI_ID>][]" value="%1$s" multiple> %1$s',
    'image' => '<img style="max-width:100px;max-height:100px" src="%s" alt=""/>',
    'i' => '<img style="max-width:100px;max-height:100px" src="%s" alt=""/>',
    'squarethumb' => '<img style="border:0px;min-width:100px;min-height:100px;max-width:100px;max-height:100px" src="%s" alt=" "/>',
    'getimage' => '<input type="file" name="<AI_FIELD>[<AI_ID>][]" value="%1$s"><img style="max-width:100px;max-height:100px" src="%1$s" alt=" "/><input type="hidden" name="<AI_FIELD>default[<AI_ID>]" value="%1$s">',
    'getimages' => '<input type="file" name="<AI_FIELD>[<AI_ID>][]" value="%1$s" multiple><ul>list()</ul>|<li><img style="max-width:100px;max-height:100px" src="%1$s" alt=" "/></li>name="<AI_FIELD>default[<AI_ID>]" value="%1$s">',
    'images' => '<div class="gallery cf">list()</div>|<div><img src="%1$s" alt=" "/></div>'
];

$ai_token=1;

/*======================================================= actions =====================================*/

if (isset($_GET['ai_action'])){
	switch ($_GET['ai_action']){
		case 'update': 
            echo '<h1>UPDATE</h1>';
            echo ai_sql2html('SELECT * FROM '.$_GET['table'].' WHERE '.$_GET['table'].'.id ='.$_GET['id'],'update');
            break;
		case 'delete': 
            $ai_q='DELETE FROM '.$_GET['table'].' WHERE id ='.$_GET['id'];
            echo "$ai_q";
            $ai_pdo->query($ai_q);
	}
}

/*======================================================= functions ===================================*/

function ai_size_of_mask($s):int{ //CALCULE DU NOMBRE D'ARGUMENT NECESSAIRE A UN MASQUE
    preg_match_all('/(?<!%)%(%%)*(\d+\$)/',$s,$m);
    return preg_match_all('/(?<!%)%(%%)*[bcdeufFosxX]/',$s)+count(array_unique($m[0]));
    //ATTENTION size_of_mask de "%s %1$s" renvoie 2 alors que le nombre d'argument requis est 1.
}

function ai_automask($mod,$field,$type,$f,$stock,&$ai_args_to_modify,$pointer,$table,$jointarget):string{ //GENERATION DES MASQUES AUTOMATIQUE
    global $ai_predefmask;
    if (isset($pointer[$f])){
        $name=$pointer[$f];
        //echo "f=$f name=$name<br/>";
        preg_match('/^([^.]*)s\.(.*)$/',$name,$r);
    }else{
        $name=$f;
    }
    $s = "%s"; // default;
    // traitement des masques automatiques par nom de champ
    //echo "<br/>mod=$mod<br/>";
    if ($mod=='select'){
        if ($f=='image')
            $s=$ai_predefmask['image'];
        if ($f=='images')
            $s=$ai_predefmask['images'];
    }else{ // pour la mise  jour, on ne peut pas mettre à jour l'image autrement qu'à la main
        if ($f=='image')
            $s=$ai_predefmask['getimage'];
        if ($f=='images')
            $s=$ai_predefmask['getimages'];
    }
    //echo "$s<br/>";
    if($mod!="select" and $s=='%s'){
        $stock +=1;
        //echo "field=$field<br/>";
        //show($jointarget,'jointarget in automask');
        //echo "dans automask type=$type<br/>";
        switch($type){
            case (preg_match('/^id\_(.*)$/',$type,$options) ? true : false) : // cas des champs clé étrangères id_truc, id_truc_machin
                $x=explode('_',$type);
                //echo "type=$type<br/>";
                $target_field=$x[1];
                //echo "target field=$target_field<br/>";
                $name = $type; // cas particulier pour le champ à modifier
                //echo "SELECT id,$target_field FROM $target_field"."s order by $target_field";
                $s = "%s<select name=\"$name".'[<AI_ID>][]">'.ai_sql2html("SELECT id,$target_field FROM $target_field"."s order by $target_field",'select',[$ai_predefmask['option']],'v',['%s','%s','%s'],[],0).'</select>';
                //echo "<xmp>S=$s</xmp>";
                break;

            case (preg_match('/^int\((.*)\)$/',$type,$r) ? true : false) :
                $s = '<input type="text" name='.$name.'[<AI_ID>][] size="'.$r[1].'" value="%s" />';
                break;
            case 'date':$s = '<input type="date" name='.$name.'[<AI_ID>][] value="%s" />';
                break;
            case 'text':
            case 'mediumtext':
            case 'longtext':
                $s = '<textarea rows="4" cols="15" name="'.$name.'[<AI_ID>][]" value="%1$s">%1$s</textarea>';
                break;
            case 'tinytext':
                $s = '<input type="text" name="'.$name.'[<AI_ID>][]" size="15" value="%s"/>';
                break;
            case (preg_match('/^varchar\((.*)\)$/',$type,$r) ? true : false) :
                $s = '<input type="text" name='.$name.'[<AI_ID>][] size="'.$r[1].'" value="%s" />';
                break;

            case (preg_match('/^set\((.*)\)$/',$type,$options) ? true : false) :
                $s = '%s';
                foreach(explode(',',$options[1]) as $option){
                    $option = str_replace("'","",$option);
                    $s .= '<input type="checkbox" name="'.$name.'[<AI_ID>][]" value="'.$option.'">'.$option.'<br>';
                }
                break;

            case (preg_match('/^enum\((.*)\)$/',$type,$options) ? true : false) :
                $s = '%s';
                foreach(explode(',',$options[1]) as $option){
                    $option = str_replace("'","",$option);
                    $s .= '<input type="radio" name="'.$name.'[<AI_ID>][]" value="'.$option.'">'.$option.'<br>';
                }
                break;

            default : $s = '(default)%s'; break;
        }
    } 
    if (ai_size_of_mask($s)>$stock){
        error("Too few data for the mask.");
    }
    else{
        $ai_args_to_modify[] = $name;
        //echo "<xmp>$s</xmp>";
        return $s;
    }
}

function ai_cellule_check($cellule):string{
    //echo "<b>cellule AVANT</b>:<xmp>$cellule</xmp><br/>";
    $cellule=str_replace("\n",'SauTdeLigne',$cellule);// sinon le pregmatch ne prend qu'avant le saut de ligne...
    //if(preg_match('/^([^<]*)(<input type="(?:checkbox|radio)".*)$/',$cellule,$a)){
    if(preg_match('/^([^<]*)(.*)(<input type="(?:checkbox|radio)".*)/',$cellule,$a)){
        //echo "je suis dedans";
     //   echo "avant : $cellule <br/>";
        $os=explode(',',$a[1]);
        //show($os,'OS');
        $cellule=$a[2].$a[3];
        foreach($os as $o){
           $cellule = str_replace("value=\"$o\"", "value=\"$o\" checked ",$cellule);
        }
      //  echo "après : $cellule <br/>";
      $cellule=str_replace('SauTdeLigne',"\n",$cellule);
      //echo "<b>cellule APRES</b>:<xmp>$cellule</xmp><br/>";
        return $cellule;
    }
    //echo "<hr/>*1*$cellule***";
    //echo "<hr/>*2*<xmp>$cellule</xmp>***";
    $cellule =  preg_replace('/^([^<]+)(.*)>\1/','\2 selected>\1',$cellule); // pour les select
    //$cellule =  preg_replace('/^([^<]+)(.*)value="\1"/','\2value="\1" checked',$cellule); // pour 
    //echo "<hr/>*3*<xmp>$cellule</xmp>***";
    $cellule=str_replace('SauTdeLigne',"\n",$cellule);
    //echo "<b>cellule APRÈS</b>:<xmp>$cellule</xmp><br/>";
    return $cellule;
}

function ai_get_query_args($q):array{ //RECUPERATION DES INFORMATIONS RELATIVES AUX TABLES DE REQUETE SQL
    global $ai_pdo;
    $ai_pdo->query("DROP VIEW IF EXISTS mv");
    //echo "view: $q<br/>";
    $ai_pdo->query("CREATE VIEW mv AS $q");
    $data=$ai_pdo->query("DESCRIBE mv");
    $result=[];
    while ($line=$data->fetch()){
        $result[]=$line;
    }
    //show($result,'result dans ai_get_query_args');
    return $result;
}

function listbr($q){ // return first column with <br/> separator (for vertical writing in <hr>  
    global $ai_pdo;
    $data = $ai_pdo->query($q);
    $r = [];
    while ($line = $data->fetch(PDO::FETCH_NUM)){
        $r[] = $line[0];
    }
    return join('<br/>',$r);
}
    
function ai_flip($arr):array{ //INVERSION DES LIGNES ET COLONNES D'UN TABLEAU BIDIMENSIONNEL
    $out = array();
    //show($arr,'arr');
    foreach ($arr as $key => $subarr){
        foreach ($subarr as $subkey => $subvalue){
            $out[$subkey][$key] = $subvalue;
        }
    }
    return $out;
}

function ai_request2ai(&$q,&$arg_field_arr,&$pointer,$mod,$table1,&$jointarget){ // Traite la requête pour gérer les normes spécifiques d'ai
    // "duck.naissance>villes.fondateur>gugusse.nom" transformation en jointure
    // select ...  ALIAS1.nom ... join gugusse as ALIAS1 on ALIAS2.fondateur=ALIAS1.id join ville as ALIAS2 on duck.naissance=ALIAS2.id 
    global $ai_pdo,$ai_token,$ai_label_ala_lisp;
    
    //echo "q à l'entrée de request2ai=$q<br/>";
    preg_match_all('/^SELECT (.*) FROM ([^, ]*).*$/',$q,$arg_field_arr);
    //show($arg_field_arr,'show_field_arr');
    $t1=$arg_field_arr[2][0];
    //echo "t1=$t1";
    $arg_field_arr=explode(',',$arg_field_arr[1][0]);
    
    foreach ($arg_field_arr as &$c){ // transforme les id_truc (clés étrangères) en a.b>c.d
    	if (substr($c,0,3) == 'id_'){
        	$x=explode('_',$c);
                $c="$t1.$c>".$x[1].'s';
                if (sizeof($x)<4){
                    $c.=".".$x[1];
                }else{
                    $c.=".".$x[3];
                }
        }
    }
    //show($arg_field_arr,'arg_field_arr');
    foreach($arg_field_arr as &$arg){
        // on calcule la liste des champs de la table, on en aura besoin à la fin
        $data = $ai_pdo->query("select distinct(COLUMN_NAME) from information_schema.COLUMNS where TABLE_NAME='$t1';");
                while ($line = $data->fetch(PDO::FETCH_NUM)){
                $listedeschamps[] = $line[0];
            }
        //show($listedeschamps);
        //echo "arg=$arg<br/>";
        $arg_arr = explode('>',$arg); // attention si on a des comparaisons dans une une fonction.
        $firstw = explode('.',$arg_arr[0])[0];
        //echo "firstw=$firstw<br/>";
        //show($arg_arr,'arg_arr');
        // pour éviter ce cas, il faut que le premier terme soit la table1
        // A.B>C.D>E.F  select A1.F      JOIN C as A1 ON A.B=A1.id
        //                               JOIN E as A2 on A1.D=A2.id 
        $join = null;
        $f=[];
        if (count($arg_arr)!=1 and ($firstw == $t1)){ // si le découpage par > est ok pour des champs
            //show($arg_arr,'arg_arr');
            $queue=explode('.',end($arg_arr));
            $queue=$queue[1];
                        // field to show
            for($i=0;$i<count($arg_arr)-1;$i++){
                $a=explode('.',$arg_arr[$i]);
                $f[]=$a[1];
                $a1=explode('.',$arg_arr[$i+1]);
                if($i == 0){
                    $aliasfirst = $a[0];
                }else{
                    $aliasfirst = 'ALIAS'.($ai_token-1);
                }
                $join .= ' JOIN '.$a1[0]." as ALIAS$ai_token ON $aliasfirst.$a[1] = ALIAS$ai_token.id ";
                $jointarget[$ai_token] = $a1[0]; // alias<-->jointure table
                $ai_token++;
            }
            //show($arg,'arg');
            //show($join,'join');
            //show($f,'f');
            $f[] = $queue;
            $n=$f[0];
            if ($ai_label_ala_lisp){
                $n = $f[$i]."(".$n.")";
            }else{
                $n=explode('_',$f[0]);
                $n=$n[sizeof($n)-1];
            }
            //echo "le nom est: $n<br/>";
            if ($mod!='select'){
                $name=explode('.',$arg_arr[0]);
                $pointer[]=$name[1];
                //$pointer["ALIAS".($ai_token-1)]=$arg_arr[0];
            }else{
                $pointer[]=$arg;
            }
            $arg="ALIAS".($ai_token-1).".$queue as '$n' ";
        }
        else { // ajout éventuel de la table1 pour éviter les ambiguités
            $pointer[]=$arg;
            $arg_arr = explode('.',$arg);
            //if (count($arg_arr)==1){
            // met la table en regard sauf si c'est une fonction comme sum,group_concat, etc...
            //echo "table=$t1<br/>";
            if (in_array($arg,$listedeschamps)) // seulement les champs, parce qu'il peut y avoir des fonctions
                $arg="$t1.$arg";               // attention s'il y avait plusieurs tables!
            //}
        }
    //show($arg_field_arr,'arg_field_arr à la fin');
    $q=preg_replace('/^(SELECT ).*( FROM [^ ]*)(.*)$/','$1'.join(',',$arg_field_arr)."$2$join$3",$q);
    }
    //echo "$q à la fin de request2ai<br/>";
}

function ai_develop_star(&$q,$table1,$mod){ // in case of *, develop and in case of id_truc convention, transform in a.b>c.d conventions, and remove id 
    global $ai_pdo;
     // cas du select * qu'il faut développer
    $args = ai_get_query_args($q,$ai_pdo);
    $fields = array_column($args,"Field");
     if ($mod == 'insert'){
         array_shift($fields); // remove the id        
     }
    $fields = implode(',',$fields);
    //echo "FIELDS=$fields<br/>";
    $q=str_replace('SELECT *','SELECT '.$fields,$q);
    return $q;
}

function ai_xmp($t){ // string or array
  if (is_array($t))
    return '<xmp>['.join(',',$t).']</xmp>';
  else
    return "<xmp>$t</xmp>";
}

/* to paginate the function ai_sql2html */

function ai_arg_to_pass($name,$value){
    return "<input type='hidden' name='$name' value='".serialize($value)."'/>\n"; 
    }

function ai_sql2html_pagin($page,$q,$mod='select',$m=[],$o='h',$e='default_envelop'){
global $ai_token;
    $ai_token++;
    $token=$ai_token;
    preg_match('/^SELECT (.*?)(FROM.*?)(LIMIT(.*?))?(OFFSET(.*?))?$/',$q,$r);
    //show($r);
    if (isset($r[3])){
        $limit = $r[4];
    }else{
        //echo "SELECT count(*) $r[2]<br/>";
        $result = ai_simple_query("SELECT count(*) $r[2]");
        $limit = $result[0];
    }   
    if (isset($r[5])){
        $offset = $r[6];
    }else{
        $offset = 0;
    }
    $q = "SELECT $r[1]$r[2] LIMIT $page OFFSET $offset";
    //alert(document.getElementById('offsetmodifcible$ai_token').innerHTML);s
return "<form method='post' action='' onclick=\"submitForm(this,'cible$token');sorttable.init;\">\n".
  ai_arg_to_pass('q',$q).
  ai_arg_to_pass('mod',$mod).
  ai_arg_to_pass('m',$m).
  ai_arg_to_pass('o',$o).
  ai_arg_to_pass('e',$e).
  "<input type='hidden' name='pagination'/>
  <input type='hidden' value='$token' name='token' />
  <input type='hidden' value='$offset' name='offsetinitial'/>
  <input type='hidden' value='$limit' name='limit' />
  <input type='hidden' value='$page' name='page' />
  <input type='button' value='|<'/>
  <input type='button' value='<<'>
  <input type='button' value='<'>   
  <input type='button' value='>'> 
  <input type='button' value='>>'> 
  <input type='button' value='>|'>
  </form>
  <div id='cible$token'>".ai_sql2html($q,$mod,$m,$o,$e).
  "<div style='display:none' id='offcible$token'>$offset</div>
  </div>";
}


/*============================================== fonction principale ====================*/

function ai_sql2html($q,$mod='select',$m=[],$o='h',$e='default_envelop',$th=[],$display=1){ //FONCTION PRINCIPALE
    global $ai_pdo,$ai_predefmask,$ai_debug,$ai_demo_mode,$ai_info,$ai_envelop,$ai_token,$ai_readable_mod;
    // intule de continuer s'il n'y a aucune réponse dans la base
    $test=preg_replace('/^SELECT (.*?) FROM (.*)$/',"SELECT count(*) FROM $2",$q);
    if (ai_simple_query($test)[0]==0)
        return '';
    if (!is_array($m)){
        $m=str_split($m,1); //convertit en un tableau de caractères - pour aller plus vite à écrire les tableaux de masques
    }
    // affichage des paramètres d'entrée
    if ($q[0] == 's'){
        error("Mettre les mots clé sql en majuscules!");
        exit;
    }
    if ($display and $ai_info){ // display his own parameters
	$s="
    REQUÊTE   $q<br/>
       MODE   $mod<br/>
    MASQUES   ".ai_xmp($m)."<br/>
ORIENTATION   $o<br/>
  ENVELOPPE   ".ai_xmp($e);
	echo sprintf('<div style="background: lightgreen">%s</div>',$s);
    }
    if (!is_array($e)){
        //show($e,'enveloppe');
        if ($mod!='select' and $e == 'default_envelop'){
            $e = 'form_envelop';
        }
        $e=$ai_envelop[$e];
    }
    //show($m,'masques à l\'entrée de ai_sql2html');
    $table1 = preg_replace('/^SELECT(.*)FROM ([^ ]*)(.*)$/','$2',$q);
    $realfields=[];
    if (preg_match('/^SELECT \*(.*)$/',$q)){
        $q = ai_develop_star($q,$table1,$mod,$realfields); // $realfields comme l'indique son nom (id_ville par ex)
    }
    if ($mod == 'update'){
        if (!preg_match('/SELECT id,/',$q)){ // ensure an id is given for update
            $q=str_replace('SELECT ','SELECT id,',$q);
            }
        array_unshift($m,$ai_predefmask['comment']); // comment the id
    }
    //show($m,"après traitement update");
    //echo "after develop_star $q<br/>";
    ai_request2ai($q,$field,$pointer,$mod,$table1,$jointarget);
    //echo "query after requet2ai 1: <b>$q</b> <hr/>";
    //echo "<b>NEW</b> : $q";
    $table1 = preg_replace('/^SELECT(.*)FROM ([^, ]*)(.*)$/','$2',$q);
    $query_arg = ai_get_query_args($q);
    $arg_field = array_column($query_arg,"Field");
    $arg_type = array_column($query_arg,"Type");
    //show($arg_field,'arg_fields');
    //show($arg_type,'arg_type');
    //show($pointer,'pointer');
/* TODO */
    for ($i=0;$i<sizeof($arg_field);$i++){ // pas de ruse!
         if ($pointer[$i]!=$arg_field[$i]){
             $arg_type[$i]=$pointer[$i];
         }
     }
    $arg_total = count($query_arg);
    $arg_used = 0;
    $m_size = [];
    
    //show($arg_field,'argfield'); // les arguments
    //show($pointer,'pointer');
    //show($arg_type,'arg_type');    
    $tab=[];
    if ($mod=='insert'){ // génération d'un tableau vide
        if (preg_match('/(.*)LIMIT (.*)/',$q,$r)){ //  on récupère la veleur de LIMIT
            $limit = $r[2];
            //echo "LIMIT=$limit";
            $emptyrecord = [];
            foreach ($arg_field as $arg){
                $emptyrecord[$arg]='';
            }
            $tab = array_fill(0,$limit,$emptyrecord);
	    }else{
            error("No limit in INSERT");
        }
    }else{
        //echo "q avant récupération des données = $q<br/>"; 
    	$data = $ai_pdo->query($q); // get data from sql
        while ($line = $data->fetch(PDO::FETCH_ASSOC)){
            $tab[] = $line;
        }
   }
   //show($tab,'tab');
    //show($m,'masques à l\'entrée des traitements préliminaires');
    $ai_args_to_modify = [];
    //show($m,'masques');
    foreach($m as &$mask){ //Traitement preliminaire des masques
        //echo $mask;
        $cible =explode('.',$field[$arg_used]);
        //show($cible,'cible');
        if (isset($cible[1]))
            $cible = $cible[1]; // le champ cible (pour ai_arg_to_modify) - n'existe pas pour les fonctions
        $tmp = ai_size_of_mask($mask);
        if($arg_used==$arg_total){
            error("break arg_used($arg_used)==arg_total($arg_total) - vérifiez vos masques trop gourmands");break;
            }
        //echo "<b>mask</b>=*$mask*<br/>";
        if ($tmp==0){
            if($mask=='*'){
                $mask = ai_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$ai_args_to_modify,$pointer,$table1,$jointarget); 
                $tmp = ai_size_of_mask($mask);
                //echo "tmp * =$tmp<br/>";               
            }else{
               if ($mask[0] == '='){ // affectation forcée
                $value=substr($mask,1,strlen($mask)-1);
                $mask = "<input type='hidden' name='<AI_FIELD>[][]' value='$value'/>$value<!--%s-->";
                $ai_args_to_modify[] = $cible;
                $tmp = ai_size_of_mask($mask); 
                //echo "tmp affectation forcée=$tmp<br/>";
            }else{
               if (isset($ai_predefmask[$mask])){
                    $mask = $ai_predefmask[$mask];
                    $ai_args_to_modify[] = $cible; // à tout hasard ce peut être un champ imput
                    $tmp = ai_size_of_mask($mask);
                    //echo "tmp masque prédéfini= $tmp<br/>";
                }
            }
          }
        }
        if ($mask[0] == '?'){
            //echo "$mask";// traitement des masques conditionnels contenant des masques automatiques ou prédéfinis
            if (preg_match('/\^?\((.*)=(.*)\):(.*):(.*)$/',$mask,$r)){
                $x="\$r = ($r[1]=$r[2]) ? $r[3] : $r[4];";
                //show($r,'r');
                //echo "$mod<xmp>mask1=$mask</xmp><hr/>";
                $mask=str_replace('*',ai_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$ai_args_to_modify,$pointer,$table1,$jointarget),$mask);
                if ($r[3] != '*' and isset($ai_predefmask[$r[3]])){
                    $mask=str_replace($r[3],$ai_predefmask[$r[3]],$mask);
                }
                //echo "r3=*$r[3]*<br/>";
                if ($r[4] != '*' and  isset($ai_predefmask[$r[4]])){
                    $mask=str_replace($r[4],$ai_predefmask[$r[4]],$mask);
                }
                //echo "<xmp>mask2=$mask</xmp><hr/>";
                $tmp = (ai_size_of_mask($mask) / 2); // division par deux car il y a sioui et sinon
                //echo "tmp masque conditionnel=$tmp<br/>";
                //echo "tmp=$tmp<br/>";
            }else{
                error('Erreur de syntaxe dans un masque conditionnel');
            }    
        }else{
           $tmp = ai_size_of_mask($mask);
           //echo "tmp masque fourni=$tmp pour $mask<br/>";
        }
        //echo "size of mask=$tmp <xmp>$mask</xmp><br/>";
        $m_size[]= $tmp;
        $arg_used += $tmp;
    }
    $m = array_slice($m,0,count($m_size));
    //echo ('masques: <xmp>'.join(',',$m).'</xmp>');
     
    //echo "arg used=$arg_used / $arg_total";
     
    while($arg_used<$arg_total){ //Ajout des masques automatiques
        $m[] = ai_automask($mod,$field[$arg_used],$arg_type[$arg_used],$arg_field[$arg_used],$arg_total-$arg_used,$ai_args_to_modify,$pointer,$table1,$jointarget);
        $m_size[] = ai_size_of_mask(end($m));
        $arg_used += end($m_size);
    }
        //show($m,'masques');
        //show($data,'data');
    $flines = null;
    $s = [];
    if (isset($e[3])){ // si enveloppe avec un champ pour les th
        //show($arg_field,'arg_field');
        $nc=0; // numéro de champ
        $vireleid=0;
        if ($mod == 'update'){
            $vireleid=1;
            $nc=1;
        }
        for ($i=0+$vireleid;$i<sizeof($m);$i++){ 
            //echo "msize = $m_size[$i]<br/>";
            if (isset($th[$i])){
                if ($th[$i]!='h'){ // si le champ n'est pas masqué
                    if (preg_match('/QUERY(.*)\/QUERY/',$th[$i],$r)){ // listes spéciales affichées verticalement
                        //show($r,'résultat de la requete');
                        $th[$i] = str_replace('QUERY'.$r[1].'/QUERY',"<div style='white-space:nowrap;' class='vertical'>".listbr($r[1])."</div>",$th[$i]);
                    }
                $s[] = sprintf($e[3],$th[$i]);
                }
            }
            else
                $s[] = sprintf($e[3],$arg_field[$nc]);
            $nc=$nc+$m_size[$i];
            }
        $flines[] = $s;
    }
    
    //show($m_size,'m_size');
    
    //show($flines,'flines');
           
    $n=0;
    foreach($tab as $line){ //Traitement du tableau
        $i=0;
        $fcells = null;
        //echo show($m,'masques');
        //show($ai_args_to_modify,'to modifiy');
        foreach($m as $k=>$v){// Pour chaque masque, mise en place des cellules avec les données (de $line)
            if (!($i == 0 and $mod == 'update')){ // on ne prend pas la colonne des id pour les updates
                //echo "<xmp>k=$k v=$v</xmp><br/>   ";
                //echo "<xmp>cell=$cell</xmp><br/>";
                if ($v[0] == '?'){ // traitement des masques conditionnels de la forme ?(condition):si oui:si non
                    if (preg_match('/\?\((.*)=(.*)\):(.*):(.*)$/',$v,$r)){
                        //show($r,'r');
                        //echo " condition=(".$line[$r[1]]." == $r[2]) sioui=$r[3] sinon=$r[4]<br/>";
                        //echo "*".$line[$r[1]]."*".$r[2]."   *<br/>";
                        $condition=($line[$r[1]]==$r[2]);
                        //echo $condition;
                        if ($line[$r[1]]==$r[2]){
                            $masque=$r[3];
                        }else{
                            $masque=$r[4];
                        }
                        //echo "$masque<br/>";
                        //echo "<xmp>cell=$cell</xmp>";
                        $cell = vsprintf($masque,array_slice($line,$i,$m_size[$k]));
                    }else{
                        error("Syntaxe incorrecte dans le masque conditionnel $v");
                    }
                }else{ // the lists traitment // TODO
                    if (preg_match('/^(.*)list\(\)(.*)\|(.*)$/',$v,$r)){ // for one parameter only for the moment
                        $dd = explode("\n",join('',array_slice($line,$i,$m_size[$k])));
                        foreach($dd as &$d){
                            $d=sprintf($r[3],$d);
                        }
                        $dd = join('',$dd);
                        $cell = $r[1].$dd.$r[2];
                    }
                    else{
                        $cell = vsprintf($v,array_slice($line,$i,$m_size[$k])); // given mask
                    }
                }
                $ai_token++;
                $cell = str_replace('<AI_TOKEN>',$ai_token,$cell);
                $cell = str_replace('<AI_FIELD>',$arg_field[$i],$cell);
                if ($mod=='update'){ 
                    $cell = str_replace('<AI_ID>',$line['id'],$cell);
                    $cell=ai_cellule_check($cell);             
                }
                if ($mod=='insert'){
                    $cell=ai_cellule_check(str_replace('<AI_ID>','',$cell));             
                }
                if (preg_match('/(.*)<input (.*)/',$cell)){
                    //echo "ligne $n à modifier<br/>";
                }
                if ($v != '<!--%s-->')  // le champ spécial 'h' masque toute la colonne (avec son masque d'enveloppe)
                    $fcells[] = sprintf(str_replace('<AI_FIELD>',$arg_field[$i],$e[2]),$cell);
            }
            $i += $m_size[$k];
        }
        $flines[] = $fcells;
        $n++;
        //ajout de chaque lignes
    }

    if($o=='v'){//Rotation du tableau
        $flines = ai_flip($flines);
    }
    
    //show ($flines,'flines');

    $ftable = null;
    foreach($flines as $l){//Mise en forme du resultat
        //show($l,'l=');
        $ftable .= sprintf($e[1],join('',$l));
        //ajout des balises de ligne
    }
    $hidden_data=[]; // to give args_to_modify to the forms (update/insert)
    if($mod!='select'){//Installation des ids cachés pour update, et des args_to_modify
        $hidden_data[] = '<input type="hidden" name="ai_table" value="'.$table1.'"/>';
        foreach($ai_args_to_modify as &$arg){
            //echo "arg ancien=$arg<br/>";
            if (isset($pointer[$arg])){
                //echo "arg=$arg => ".$pointer[$arg]."<br/>";
                $arg=$pointer[$arg];
                //echo "arg nouveau=$arg<br/>";
            }
            $hidden_data[] = "<input type='hidden' name='ai_args_to_modify[]' value='$arg'/>";
        }
    }
    // opérateur de répétition (pour les tableaux en Markdow ou en LaTeX
    $e[0] = str_replace(array('<AI_NBCOL>','<AI_HIDDEN_DATA>','<AI_MOD>'),array(sizeof($m),join('',$hidden_data),"ai_$mod"),$e[0]);
    $e[0] = preg_replace_callback('/REP\((.*),(.*)\)/U',function ($m){
                                                            return str_repeat($m[1],$m[2]);
                                                            },$e[0]);
    $html = sprintf($e[0],$ftable);
    if ($display and $ai_info){ // display his own parameters at the end
	$s="
    REQUÊTE   $q<br/>
       MODE   $mod<br/>
    MASQUES   ".ai_xmp($m)."<br/>
ORIENTATION   $o<br/>
  ENVELOPPE   ".ai_xmp($e);
	echo sprintf('<div style="background: yellow">%s</div>',$s);
    }
    return str_replace(array('<AI_TABLE>','<AI_READABLE_MOD>'),array($table1,$ai_readable_mod[$mod]),$html);
    //ajout des balises de tableau et retour du resultat
}

/*============================================== fonctions spéciales ====================*/

function ai_simple_query($q){ // renvoie la première colonne d'une requête
    global $ai_pdo;
    $result=$ai_pdo->query($q);
    return $result->FetchAll(PDO::FETCH_COLUMN);
}

function ai_query($q){
    global $ai_pdo;
    try {
        $ai_pdo->prepare($q)->execute();
    } catch (PDOException $e) {
        echo error($e->errorInfo[2]);
    }
}

function ai_files_from_form(){ // $_FILES traitement for update and insert
    global $ai_upload,$ai_uploaddir;$ai_dir_correction;
    if (sizeof($_FILES)>0){ // for the uploaded files
        $dir = "$ai_uploaddir/".$_POST['ai_table'];
        $web = "$ai_upload/".$_POST['ai_table'];
        //echo "répertoires: dir=$dir web=$web<br/>";
        if (!file_exists($dir)){ 
            mkdir($dir);
        }
        foreach($_FILES as $field=>$value){
            foreach ($value['name'] as $num=>$list){
                if (sizeof($list)!=0){
                    $liste=[];
                    for($i=0;$i<sizeof($list);$i++){
                        $file = basename($list[$i]);
                        //echo "fichier=$file<br/>";
                        //echo "tmp=".$value['tmp_name'][$num][$i]."<br/>";
                        if (move_uploaded_file($value['tmp_name'][$num][$i], "$dir/$file")){
                        $liste[]="$web/$file";
                        //show($liste,'liste des fichiers');
                        }
                    }
                    $_POST[$field][$num][$i] = join("\n",$liste); // possibly several files
                    //echo $_POST[$field][$num][$i];
                }
            }
        }
    }
}

function ai_anti_injection(&$t){
    if (is_array($t)){
        foreach($t as &$e){
            if (is_string($e)){
                $e=str_replace(array('"','\\'),array('\"','\\\\'),$e);
            }else{
                ai_anti_injection($e);
            }
        }
    }
}

function ai_update(){ //GENERATION ET EXECUTION DES REQUETES D'UPDATE (ZERO SECURITE)
    global $ai_pdo,$ai_uploaddir,$ai_upload;
    //show($_POST,'post in update');
    ai_anti_injection($_POST);
    //show($_POST,'post in update after ai_anti_injection');    
    ai_files_from_form();
    //show($_POST,'post in update');
    //show($_POST['ai_args_to_modify'],'arg to modify');
    $pre_request = "UPDATE %s SET %s WHERE id = %s";
    $new_data = ai_flip(array_slice($_POST,3,count($_POST)-3)); // sauf ai_table, ai_args_to_modify et ai_<mod>
    //show($new_data,'newdata');
    foreach ($new_data as $id => $record){  
        $modifs= [];
        foreach ($_POST['ai_args_to_modify'] as $arg){
            if (isset($record[$arg])){
            	$value=join(',',$record[$arg]);
                //echo "value=*$value*<br/>";
            	if ($value != '')
                  $modifs[] = "$arg =\"".str_replace("'","'",$value).'"';
                else
                  {
                  if (isset($record[$arg.'default'])){ //default value for file type input
                    $value=$record[$arg.'default'];
                    //echo "default=$value<br/>";
                    $modifs[] = "$arg =\"".str_replace("'","'",$value).'"';
                  }
                  }
            }   
        }
        $request = sprintf($pre_request,$_POST['ai_table'],join(' , ',$modifs),$id);
        echo "<pre>$request</pre><br>";
        ai_query($request);
    }
}

function ai_insert(){ //GENERATION ET EXECUTION DES REQUETES D'INSERT (ZERO SECURITE)
    global $ai_pdo;
    //show($_POST,'post in insert');
    ai_anti_injection($_POST);
    //show($_POST,'post in insert after ai_anti_injection');
    ai_files_from_form();
    //show($_POST,'post in insert');
    $pre_request = 'INSERT INTO %s (%s) VALUES %s';
    //echo $pre_request;
    $new_data = ai_flip(array_slice($_POST,3,count($_POST)-3)); // sauf ai_table, ai_args_to_modify et ai_data_id
    //show($new_data,'newdata');
    $fields_list=implode(',',$_POST['ai_args_to_modify']);
    //show($_POST['ai_args_to_modify'],'arg to modify');
    //echo "$fields_list<br/>";
    $values=[];
    foreach ($new_data as $record){
    //show($record);
   	$value=[];
        foreach ($_POST['ai_args_to_modify'] as $arg){
            if (!isset($record[$arg])){
                $value[] = 'NULL';
            }else{
                $la_valeur = join(',',$record[$arg]);
		if ($la_valeur == "''")
		  $la_valeur = 'NULL';
		$value[] = '"'.$la_valeur.'"';
            }
        }
        $values[]='('.join(',',$value).')';
    }
    $values=implode(',',$values);
    $request = sprintf($pre_request,$_POST['ai_table'],$fields_list,$values);
    echo "$request <br>";
    ai_query($request);
}

/*============================================== aiguillage vers une exécution au chargement de ai.php ====================*/


if (isset($_POST['ai_update'])){
    ai_update();
}

if (isset($_POST['ai_insert'])){
    ai_insert();
}

/* ============================================= réponse ajax pour la pagination ====================  */

if (isset($_POST['pagination'])){

extract($_POST);
//show($_POST);
    
//echo "offset=$offset avant<br/>";
switch ($button){
  case '|<':$offset = $offsetinitial;break;
  case '<<':$offset = max($offsetinitial,intdiv($offset,2));break;
  case '<': $offset = max($offsetinitial,$offset-$page);break;
  case '>': $offset = min($offset+$page,$limit-$page+1);break;
  case '>>': $offset = min($offset+intdiv($limit-$offset,2),$limit-$page+1);break;
  case '>|':$offset = $limit-$page+1;break; 
}

//echo 'q='.unserialize($q).'<br/>';
//echo "offset=$offset après<br/>";
preg_match('/^SELECT (.*?)(FROM.*?)(LIMIT(.*?))?(OFFSET(.*?))?$/',unserialize($q),$r);
//show($r);
$q = "SELECT $r[1]$r[2] LIMIT $page OFFSET $offset";
//echo "$q<br/>";
echo ai_sql2html($q,unserialize($mod),unserialize($m),unserialize($o),unserialize($e))."<div style='display:none' id='offcible$token'>$offset</div>";

exit; // fin du traitement ajax - on ne fait que cela
}

/*============================================== fin de la page web ====================*/

?>
