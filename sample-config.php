<?php
// database
$ai_host = 'localhost';
$ai_db   = 'database';
$ai_user = 'user';
$ai_pass = 'password';
$ai_charset = 'utf8mb4';
// for developers
$ai_debug = 1;
define('ai_DEFAULT_ORIENTATION','h');
$ai_label_ala_lisp = 1; // 1: nom(fondateur(ville)) 0: nom
$ai_upload = 'upload';
$ai_uploaddir = "/var/www/html/ai/$ai_upload";
?>
