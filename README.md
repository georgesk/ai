AI est un jeu de trois fonctions dynamiques (select, insert et create), permettant de traiter une requête sql avec une enveloppe et des masques configurables.
Il permet de produire un système de pages web dynamiques *très* rapidement.

# En deux mots

## select

**sql2html('select name,age from persons','select');**

produit:

![select](documentation/select1.png)

Il produit le code html tout seul. Vous pouvez lui demander de présenter les données dans l'autre sens.

**sql2html('select name,age from persons','select','v');**

produira:

![select](documentation/select2.png)

## update

**sql2html('select name,age from persons','update');**

produit:

![update](documentation/update1.png)

*AI* produit le code html du formulaire, et gère aussi la mise à jour lorsque l'on clique sur ok!

## create

*AI* peut aussi produire le code pour créer des enregistrements

>ai_sql2html('SELECT * FROM personnes LIMIT 1','create');

Ce qui va suivre est plus compliqué, mais vous pouvez utiliser *Ai* sans utiliser les conventions qui suivent, rassurez-vous!

-----

# Il peut aussi créer des tables automatiquement, dont vous décrivez la structure

## syntaxe

TODO pour la syntaxe de la production de code/graphviz-database

# Conventions pour les jointures automatiques

sql est un langage puissant, mais écrire des jointures est pénible. *Ai* le fait pour vous

## Convention simple: les clés étrangères automatiques

Si vous désignez vos tables d'un nom au pluriel, et que chaque enregistrement dispose 
- d'un id unique 
- d'un champ portant le même nom que la table au singulier
vous pourrez désigner des champs qui seront automatiquement l'objet d'une jointure.

### Prenons un cas simple

![jointure simple](documentation/jointure1.png)

**sql2html('select nom,id_ville from persons','select');** va faire la jointure automatique (l'id vous indiffère, vous voulez la valeur!)

et produit ceci:

![jointure simple](documentation/jointure2.png)

Ce qui revient à faire une jointure un peu fastidieuse:
> SELECT personnes.nom,ALIAS1.ville as 'ville(id_ville)' FROM personnes JOIN villes as ALIAS1 ON personnes.id_ville = ALIAS1.id 

Les familier de sql diront que c'est inutilement se compliquer la tâche... mais *AI* fait ces jointures récursivement.

Vous pourrier demander d'afficher le nom du frère du fondateur de la ville d'une personne, et vous aurez besoin des alias pour peu que plusieurs champs pointe sur la même table. Il y a ma ville de naissance, ma ville préférée etc...

### Jointures multiples sur la même table

![jointure simple](documentation/jointure4.png)

Un champ comme *id_ville* sera considéré comme pointant sur une table *villes* qui a un champ *ville*.

Mais il peut y en avoir plusieurs

Un champ comme *id_ville_naissance* sera considéré aussi comme pointant sur une table *ville* qui a un champ *ville*

## Jointures en cascade

**sql2html('select name,personnes.id_ville>villes.fondateur>fondateurs.name from personnes','select');**

produit ceci

![jointure simple](documentation/jointure3.png)

Vous auriez aimé écrire ceci à la main ?
> SELECT personnes.nom,ALIAS2.name as 'name(fondateur(id_ville))' FROM personnes JOIN villes as ALIAS1 ON personnes.id_ville = ALIAS1.id JOIN fondateurs as ALIAS2 ON ALIAS1.fondateur = ALIAS2.id 

# Mécanique des enveloppes et des masques

## Les enveloppes

## Les masques

### Le principe des masques

### Ai et les id

Les tables que peut traiter ai doivent avoir un champ id autoincrément (ce qui est assez fréquent...).
Il est à noter 
- que ce champ est enlevé des requêtes SELECT et CREATE (même lorsque * est utilité)
- qu'il est ajouté systématiqument pour les UPDATE (mais masqué)
Il est possible d'ajouter un id à une requête SELECT en écrivant la syntaxe spéciale "SELECT id,*..." pour traiter de façon spécifique par un masque cet id (par exemple utiliser le masque u (update) ou d (delete) ou ud (update+delete).

### Masques automatiques

### Masques simples

### Masques multichamps

### Masque prédéfinis

Il est évidemment possible d'étendre autant que l'on veut les masques en ajoutant des masques prédéfinis à Ai.

TODO

### Masque spécial d'affectation

Le masque '=valeur' impose une valeur à un champ.

Par exemple: 
> echo ai_sql2html('SELECT name,age FROM toons LIMIT 1','create','h',ai_FORM_ENVELOP,['=Mickey']);
affichera un formulaire permettant de donner créer un personnage en imposant son nom: *Mickey*.

### Masque conditionnel

Le masque *?(nom=Mickey):sioui:sinon* permet de produire un masque conditionnel

le champ testé doit évidemment figurer dans la requête.

la valeur de test est comparée à la valeur du premier champ demandé par le masque.
Les deux masques *sioui* et *sinon* doivent évidemment demander le même nombre de champs (ils peuvent être multichamps).

Application: une table peut ainsi n'être ouverte à la modification que pour certaines lignes. 


### Variables utilisables dans les masques

Les variables suivantes sont utilisables dans les masques. Elles seront remplacées par leur valeur au moment opportun par Ai.
- <AI_ID>       x
- <AI_FIELD>
- <NB_COL>









